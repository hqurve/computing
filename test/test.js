import {expect} from 'chai';

import {tokenize, compilers} from '../src/index.js';


let plain = [
  '2',
  '1030',
  '"hello world"',
  '[1, 2, 3, "1321"]',
  ['{hi: "world", array: ["cool"]}', {hi: "world", array: ["cool"]}],
  [';a;;2; a', 2],
  [';a;; \\x -> x; a 3', 3],
  [';a;; \\x -> \\y -> [x,y,x,y]; a 1 2', [1,2,1,2]],
  [';a;; \\x->\\ y->[x,y,x,y]; a 1 2', [1,2,1,2]],
];

let std = [
  ['2 + 3 + 4', 9],
  ['(+) 2 3 + 5', 10],
  ['6/2 * (1+3)', 12],
  [';f;;\\x -> \\y -> x^y; f 2 3 + 1.5', 8 + 1.5],
  ['2 + 3 > 3', true],
  ['13 == 13', true],
  ['true && true', true],
  ['5 > 3 && 13 == 13', true],
  ['2 + 3 > 3 && 13 == 13', true],
  [`;cool;,,,1; (- 1); cool 5`, 4],
  [`;cool;,,,1; \\x -> (- 1) $ x; cool 5`, 4],
  [`;cool;,,,1; \\x -> if (x < 3) id (cool . (- 1)) $ x; cool 5`, 2],
  [`;fib;,,,1; \\n -> if (n <= 1) id (\\n -> fib (n - 1) + fib(n - 2)) $ n; fib 23`, 28657],
  [`;g;,,,1; f . (- 1); ;f;,,,1; \\x -> if (x < 3) id g x; f 400.125`, 2.125],
  [`;f;;\\a -> \\b -> a + b; f 1 2`, 3],
];


for (let compiler_name of ['v1', 'eval']) {
  let {compile, compileStd} = compilers[compiler_name];

  describe(`compiler: ${compiler_name}`, () => {
    describe("plain: string -> output", () => {
      for (let v of plain) {
        let string, expected;
        if (Array.isArray(v)){
          string = v[0];
          expected = v[1];
        }else{
          string = v;
          expected = JSON.parse(v);
        }

        it(string, () => {
          let result = compile(tokenize(string), {});
          expect(result).to.deep.equal(expected);
        });
      }
    });


    describe("std: string -> output", () => {
      for (let [string, expected] of std) {
        it(string, () => {
          let result = compileStd(tokenize(string));
          expect(result).to.deep.equal(expected);
        });
      }
    });
  });
}
