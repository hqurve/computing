Test area at https://hqurve.gitlab.io/computing

## Data Formats
See [data formats](data_formats.md) document

## Functions

### tokenize(string)

- `string` is a string in the `Stringified` format.

Output is `Tokenized`.

### stringify(tokenized)

- `tokenized` a `Tokenized` object

Output is a string in the `Stringified format`

### compile(tokenized, aliases)

- `tokenized` a `Tokenized` object
- `aliases` an object where keys are the names of aliases and the values are `{opts: AliasOpts, value: Any}`.
Also note that the `defer` option in `AliasOpts` is ignored.

Output is called an artifact.


Note that this compile function is really the composition of an analyze function
followed by an actual compile function.

There are multiple implementations for compilation.
Currently, the artifacts of both are expected to be of similar quality
as long as lambdas are not used.

| Name | Description |
|------|-------------|
| v1             | Lambda performance is bad since the contents are compiled only after an argument is recieved |
| eval (default) | Translates code to javascript and compiles using the [Function constructor](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/Function) Performance is pretty good (~20% slower) compared to native |

