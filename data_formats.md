
## Stringified
This format is used for easy typing and sharing as it may typically be smaller than the tokenized form
and a bit easier to understand. It is heavily inspired by haskell

| Name | Format | Example |
|------|--------|---------|
| Identifier | See [LABEL_REGEX](src/tokenize.js) | `coolid` |
| Sweep      | `Value+`| `(value1 value2)` |
| Literal    | JSONString | `"hello\nworld"` |
| Array      | `\[(Value,)+\]` | `[value1, value2]` |
| Object     | `\{((JSONString \| Identifier) : Value,)+\}` |  `{k1: value1, "k\n2": value2}` |
| Lambda     | `\\Identifier -> Value` | `\x -> [x, x]` |
| Aliases    | <code>(;Identifier;<a href="#aliasopts">AliasOpts</a>;Value;)* Value</code> | <code>;g;; f . (- 1);<br/>;f;,,,1; \x -> if (x < 3) x g x;<br/>f 30.123</code> |

Note that for arrays and objects, trailing commas can be omitted.

## Tokenized
This is the de-facto format and should be the primary form for serialization and deserialization
as it can be readily converted to JSON.

Each value is one of the following

| Name | Format | Description |
|------|---------|------------|
| Identifier | `String`                                                               | Either maps to an alias or a variable |
| Sweep      | `[Value]`                                                              |                              | 
| Literal    | `{type: 'literal', value: Number\|String }`                              |                              |
| Array      | `{type: 'array', value: [Value] }`                                     | Each item in value is mapped |
| Object     | `{type: 'object', value: {prop: Value} }`                              | Each item in value is mapped |
| Lambda     | `{type: 'lambda', value: Value, variable: String}`                     |                              |
| Aliases    | `{type: 'aliases', value: Value, aliases: [String, {opts: AliasOpts? + {defer: Number}, value: Value}]}` | |

## <a name="aliasopts"></a>AliasOpts

| Property | Format | Default |Description |
|----------|--------|---------|------------|
| `precedence`    | `Number`          | `0`   | Could be positive, negative or zero. |
| `associativity` | either `>` or `<` | `>`   | |
| `gobbles`       | `[either > or <]` | `[]`  | If encountered while parsing, it would take values according to the directions for arguments |
| `defer`         | Non-negative Integer | `0` | |

Notes

- When stringified, they are concatenated by commas and trailing options may be omitted. Also, options can be left blank.

- When analyzing, if multiple differing associativities are found for the same precedence,
we choice of direction is not well defined.

- The `defer` count is only used during compilation and is used as an aid for recursive functions which may refer to themselves
before they are initialized. If this count is supplied, a defer function is temporarily placed where the final output would be
so that it may be partially used until properly initialized. For example, say we have a function `f1` which takes two arguments
<br/>
```
;f2;,,,2; \a -> \b -> if (a < 5) (f2 a) (f2 b);
```
<br/>
Then we want to set the defer count to at least two so that a function akin to `a => b => aliases.f2(a)(b)` is held in the placeholder.
Note that this would not help if within your function you call `f2(a)(b)` as it would cause infinite recursion.
Also, note that this count may not be needed depending on the compiler.


## Analyized (Not exposed)
Do not attempt to serialize/deserialize this as it contains function pointers and lambdas.
Each value is an object with `type` property and some other properties

| `type` | Properties | Description |
|--------|------------|-------------|
| `"literal"`  | `value: Any`        | native aliases are resolved and stored as literals |
| `"sweep"`    | `value: [Value\|null]` |                                                 |
| `"array"`    | `value: [Value]`    |                                                    |
| `"object"`   | `{key: Value}`      |                                                    |
| `"lambda"`   | `id: Number, value: Value` |                                       |
| `"aliases"`  | `aliases: [{id: Number, value: Value, defer: Number}], value: Value` | |
| `"variable"` | `id: Number`        |                                                    |
| `"alias"`    | `id: Number`        |                                                    |

## Compiled
Final product
