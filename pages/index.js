import './index.scss';

import * as computing from '../src/index.js';

// global.computing = computing;


function registerToggleButton(id, default_state=false){
  let button = document.getElementById(id);
  if (default_state) {
    button.setAttribute('enabled', 'true');
  }
  button.addEventListener('click', () => {
    if (button.hasAttribute('enabled')) {
      button.removeAttribute('enabled');
    }else{
      button.setAttribute('enabled', 'true');
    }
  });
  return button;
}


let btn_enable_std = registerToggleButton('btn_enable_std', true);
let btn_enable_infix = registerToggleButton('btn_enable_infix', true);
let btn_run = document.getElementById('btn_run');

let txt_editor = document.getElementById('txt_editor');
let txt_output = document.getElementById('txt_output');

btn_run.onclick = run_code;


function load() {
}
function save() {
}

function get_full_code(){
  return txt_editor.value;
}

function make_readable_output(output){
  if (Array.isArray(output)) {
    return '[' + output.map(make_readable_output).join(', ') + ']';
  }else if (typeof(output) === 'object') {
    return '{' + Object.entries(output).map(([k, v]) => k + ': ' + make_readable_output(v)).join(', ') + '}'
  }else{
    return "" + output;
  }
}
function run_code(){
  let output;
  let hasError = false;
  try {
    let string = get_full_code();
    let tokenized = computing.tokenize(string);
    try {
      output = computing.compileStd(tokenized);
    }catch(e){
      hasError = true;
      output = "Compile Error: \n" + e;
    }
  }catch(e) {
    hasError = true;
    output = "Parse Error: \n" + e;
  }

  txt_output.value = make_readable_output(output);
}

