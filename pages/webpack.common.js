import * as path from 'path';

import HtmlWebpackPlugin from 'html-webpack-plugin';

export default {
  entry: {
    index: './pages/index.js',
  },
  output: {
    filename: '_/[name].bundle.js',
    path: path.resolve('../', 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.scss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Computing',
      filename: 'index.html',
      chunks: ['index'],
      template: 'pages/index.html'
    }),
  ],
};

