
import tokenize from './tokenize.js';
import stringify from './stringify.js';
import analyze from './analyze.js';

import compileV1 from './compile-v1.js';
import compileEval from './compile-eval.js';

import * as library from './functions.js';



let inner_library = Object.fromEntries( Object.values(library).flatMap(x => Object.values(x)).flatMap(x => Object.entries(x)) );
// let aliases = [INFIX, COMFORT, BASIC].flatMap(x => Object.values(x));

function createCompiler(compile) {
  return {
    _compile: compile,
    compile: function(tokens, aliases) {
      return compile(analyze(tokens, aliases));
    },
    compileStd: function(tokens, aliases={}) {
      return compile(analyze(tokens, {...inner_library, ...aliases}));
    },
  };
}

export const compilers = {
  v1: createCompiler(compileV1),
  eval: createCompiler(compileEval),
};

export const defaultCompiler = compilers.eval;

export const compile = defaultCompiler.compile;
export const compileStd = defaultCompiler.compilerStd;


// rexports
export {
  tokenize,
  stringify,
  library,

  analyze as _analyze,
};

