
function create_defer(obj, n) {
  if (n==0) throw `n must not be 0`;

  // some specialization
  if (n == 1) return x => obj.value(x);
  if (n == 2) return x1 => x2 => obj.value(x1)(x2);
  if (n == 3) return x1 => x2 => x3 => obj.value(x1)(x2)(x3);

  let rep = args_ => x => {
    let args = [...args_, x];
    if (args.length === n) {
      let ret = obj.value;
      for (let x of args) {
        ret = ret(x);
      }
      return ret;
    }else{
      return rep(args);
    }
  };
  return rep([]);
}

function compile_aliases(aliases, context) {
  let c = {...context};
  c.aliases = {... c.aliases};
  
  for (let {id, defer} of aliases) {
    let obj = {};
    c.aliases[id] = obj;

    if (defer !== 0) {
      obj.value = create_defer(obj, defer);
    }
  }

  for (let {id, value, defer} of aliases) {
    let obj = c.aliases[id];
    if (defer !== 0) {
      obj.value = compile_rec(value, c);
    }else{
      obj.value = compile_rec(value, c);
    }
  }

  return c;
};

function compile_sweep(vs, context) {
  let args = vs.map(v => {
    if (v === null){
      return null;
    }else{
      return {value: compile_rec(v, context)};
    }
  });

  function helper(value, args) {
    if (args.length === 0) return value;
    if (args[0] !== null) return helper(value(args[0].value), args.slice(1));
    return x => helper(value(x), args.slice(1))
  }

  return helper(args[0].value, args.slice(1));
}

function compile_rec(v, context) {
  if (v.type === 'literal') {
    return v.value;
  }else if(v.type === 'sweep') {
    return compile_sweep(v.value, context);
  }else if(v.type === 'array') {
    return v.value.map(v => compile_rec(v, context));
  }else if(v.type === 'object') {
    return Object.fromEntries(Object.entries(v.value).map(([k, v]) => [k, compile_rec(v, context)]));
  }else if(v.type === 'lambda') {
    return x => { // for now, a lazy solution (no pun intended)
      let c = {...context};
      c.variables = {... c.variables}
      c.variables[v.id] = x;
      return compile_rec(v.value, c);
    };
  }else if(v.type === 'aliases') {
    let c = compile_aliases(v.aliases, context);
    // console.log(require('util').inspect(c, {depth: null}));
    return compile_rec(v.value, c);
  }else if(v.type === 'variable') {
    return context.variables[v.id];
  }else if(v.type === 'alias') {
    // console.log(require('util').inspect(context, {depth: null}));
    return context.aliases[v.id].value;
  }else{
    throw `Invalid type ${v.type}`;
  }
}

export default function compile(v){
  let context = {
    aliases: {}, // id -> {value: Any}
    variables: {}, // id -> Any
  };
  // console.log(require('util').inspect(v, {depth: null}));
  return compile_rec(v, context);
}
