const LABEL_REGEX = /^([!=._+*/<>|&$@%:\^\-]+|[a-zA-Z_][0-9a-zA-Z._]*)/;
const WHITESPACE_REGEX = /^[\s\n]*/;
const STRING_REGEX = /^"(\\.|[^"])*"/;
const NUMBER_REGEX = /^-?[0-9]+(\.[0-9]+)?([eE][+-]?[0-9]+)?/;

function tryMatch(string, i, regex, message) {
  let match = regex.exec(string.slice(i));
  if (match === null) throw `${message} at ${i}`;
  else return [i + match[0].length, match[0]];
}

function skipWhitespace(string, i) {
  return tryMatch(string, i, WHITESPACE_REGEX)[0];
}

function ensure(string, i, sub) {
  i = skipWhitespace(string, i);
  if (!string.slice(i).startsWith(sub)) throw `expected ${sub} at ${i}`;
  return i + sub.length;
}
function hasNext(string, i, sub) {
  return string.slice(i).startsWith(sub);
}

// simple parsers (used in more than one place)
function parseLabel(string, i) {
  i = skipWhitespace(string, i);
  return tryMatch(string, i, LABEL_REGEX, "expected label");
}
function parseString(string, i) {
  let str;
  i = skipWhitespace(string, i);
  [i, str] = tryMatch(string, i, STRING_REGEX, "expected string");
  return [i, JSON.parse(str)];
}
function parseNumber(string, i) {
  let number;
  i = skipWhitespace(string, i);
  [i, number] = tryMatch(string, i, NUMBER_REGEX, "expected number");
  return [i, JSON.parse(number)];
}

// Expressions
function parseTerm(string, i) {
  let term;
  if (string[i] === '(') { // parenthesis
    i = i + 1;
    [i, term] = parseExpression(string, i);
    i = ensure(string, i, ')');
  }else if(string[i] === '[') { // array
    i = i + 1;

    let array = [];

    i = skipWhitespace(string, i);
    while (string[i] !== ']') {
      let expr;
      [i, expr] = parseExpression(string, i);
      array.push(expr);

      i = skipWhitespace(string, i);
      if (string[i] === ']') break;
      i = ensure(string, i, ',');
      i = skipWhitespace(string, i);
    }
    i = i + 1;
    term = {type: "array", value: array};
  }else if(string[i] === '{') { // object
    i = i + 1;

    let object = [];

    i = skipWhitespace(string, i);
    while (string[i] !== '}') {
      let key, expr;

      if (string[i] === '"') {
        [i, key] = parseString(string, i);
      }else{
        [i, key] = parseLabel(string, i);
      }

      i = ensure(string, i, ':');

      [i, expr] = parseExpression(string, i);
      object[key] = expr;

      i = skipWhitespace(string, i);
      if (string[i] === '}') break;
      i = ensure(string, i, ',');
      i = skipWhitespace(string, i);
    }
    i = i + 1;
    term = {type: "object", value: object};
  }else if(NUMBER_REGEX.test(string[i])) { // number
    let number;
    [i, number] = parseNumber(string, i);
    term = {type: 'literal', value: number};
  }else if(string[i] === "\"") { // string in JSON format
    let str;
    [i, str] = parseString(string, i);
    term = {type: 'literal', value: str};
  }else if (string[i] === '\\') {
    i = i + 1;
    let variable, value;
    [i, variable] = parseLabel(string, i);
    i = ensure(string, i, '->');
    [i, value] = parseExpression(string, i);
    term = {type: 'lambda', value: value, variable: variable};
  }else{ // just a label
    [i, term] = parseLabel(string, i);
  }
  return [i, term];
}
function parseExpression(string, i) {
  let terms = [];
  let aliases = [];

  i = skipWhitespace(string, i);
  while (string[i] ===';') {
    let alias;
    [i, alias] = parseAlias(string, i);
    aliases.push(alias);
    i = skipWhitespace(string, i);
  }

  let end_tokens = ",;])}";

  while(true) {
    i = skipWhitespace(string, i);
    if (string.length === i || end_tokens.includes(string[i])) break;

    let term;
    [i, term] = parseTerm(string, i);
    terms.push(term);
  }

  if (terms.length === 0) throw `empty expression at ${i}`;

  let ret;
  if (aliases.length === 0) {
    ret = terms;
  }else{
    ret = {type: 'aliases', value: terms, aliases: aliases};
  }
  return [i, ret];
}

function parseAliasOpts(string, i){
  let props = [
    ['precedence', NUMBER_REGEX, x => JSON.parse(x)],
    ['associativity', /^[<>]/, x => x],
    ['gobbles', /^[<>]*/, x => [... x]],
    ['defer', /^[0-9]+/, x => JSON.parse(x), x => Number.isInteger(x) && x >=0],
  ];

  let opts = {};
  
  for (let [prop, regex, f, filter] of props) {
    i = skipWhitespace(string, i);
    if (string[i] === ';') break;

    i = skipWhitespace(string, i);
    if (string[i] !== ',') {
      let v;
      [i, v] = tryMatch(string, i, regex);
      v = f(v);
      if (filter !== undefined && !filter(v)) {
        throw `bad ${prop}: ${v}`;
      }
      opts[prop] = v;
    }
    i = skipWhitespace(string, i);

    if (string[i] === ';') break;
    i = ensure(string, i, ',');
  }

  return [i, opts];
}

function parseAlias(string, i) {
  let label, expression;
  let opts;

  i = ensure(string, i, ";");

  [i, label] = parseLabel(string, i);

  i = ensure(string, i, ";");

  [i, opts] = parseAliasOpts(string, i);
  
  i = ensure(string, i, ';');

  [i, expression] = parseExpression(string, i);

  i = ensure(string, i, ";");

  return [i, [label, {opts: opts, value: expression}]];
}

export default function tokenize(string) {
  let [i, expression] = parseExpression(string, 0);

  i = skipWhitespace(string, i);
  if (i < string.length) throw `parsing stopped at ${i}`;

  return expression;
}
