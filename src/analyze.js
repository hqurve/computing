const PRECEDENCE_STEP = 10;

// [Value] -> SweepValue (with no opts set)
function analyze_sweep_(vs) {
  let operation_info = [... new Map(vs.map(v => [v.opts?.precedence, v.opts?.associativity]))]
    .filter(a => a[0] !== undefined)
    .sort((a,b) => b[0] - a[0])
  ;

  for (let [p, d] of operation_info) {
    let incoming;
    let outgoing = [];

    if (d === '>') {
      incoming = vs;
    }else if (d === '<'){
      incoming = vs.reverse();
    }else{
      throw `invalid direction ${d}`;
    }
    vs = undefined;

    while (incoming.length !== 0){
      let v = incoming.shift();

      if (v.opts?.precedence !== p) {
        outgoing.push(v);
        continue;
      }

      let args = [];
      for (let d_ of v.opts.gobbles) {
        if (d === d_) {
          if (incoming.length !== 0){
            args.push(incoming.shift());
          }else{
            args.push(null);
          }
        }else{
          if (outgoing.length !== 0) {
            args.push(outgoing.pop());
          }else{
            args.push(null);
          }
        }
      }

      outgoing.push({type: 'sweep', value: [v, ... args]});
    }

    if (d === '>'){
      vs = outgoing;
    }else{
      vs = outgoing.reverse();
    }
  }

  return {type: 'sweep', value: vs};
}

function analyze_sweep(vs) {
  let get_prec = v => Math.floor((v.opts?.precedence ?? 0) / PRECEDENCE_STEP);
  // Node = {prec: Number, vs: [Value], prev: Node, next: Node}
  // Invariant: parent_prec < children
  let tree = {prec: get_prec(vs[0]), vs: [vs.shift()], prev: undefined, next: undefined};

  // populate tree
  for (let v of vs) {
    let prec = get_prec(v);

    function add_to_tree(current, v) {
      if (current.prec < prec) {
        if (current.next === undefined) {
          current.next = {prec: prec, vs: [v]};
        }else{
          add_to_tree(current.next, v);
        }
      }else if (current.prec === prec && current.next === undefined){
        current.vs.push(v);
      }else{// replace node in place
        let node = {...current};

        current.prec = prec;
        current.vs = [v];
        current.prev = node;
        current.next = undefined;
      }
    }
    add_to_tree(tree, v);
  }

  // collapse tree
  // Returns [value]
  function collapse(node, prec=undefined) {
    if (node === undefined) return [];
    
    let left = collapse(node.prev, node.prec);
    let right = collapse(node.next, node.prec);

    let vs = [...left, ...node.vs, ...right];

    if (node.prec === prec){
      return vs;
    }else{
      return [analyze_sweep_(vs)];
    }
  }

  return collapse(tree)[0];
}


// returns a value
// identifiers is an object which maps to Value. Values can also have the `opts` parameter for AliasOpts.
function analyze_rec(v, identifiers, id_counters) {
  if(typeof(v) === 'string') {
    if (v in identifiers) {
      return identifiers[v];
    }else{
      throw `Unknown identifier ${v}`;
    }
  }else if(Array.isArray(v)) { // sweep
    return analyze_sweep(v.map(x => analyze_rec(x, identifiers, id_counters)));
  }else if(typeof(v) === "object"){
    if (v.type === "literal") {
      return {type: 'literal', value: v.value};
    }else if (v.type === "array") {
      return {type: 'array', value: v.value.map(x => analyze_rec(x, identifiers, id_counters))};
    }else if (v.type === "object") {
      return {type: 'object', value: Object.fromEntries(Object.entries(v.value).map(([k, x]) => [k, analyze_rec(x, identifiers, id_counters)])) };
    }else if (v.type === "lambda") {
      identifiers = {... identifiers};

      let id = ++id_counters.variables;
      identifiers[v.variable] = {type: 'variable', id: id};
      return {type: 'lambda', id: id, value: analyze_rec(v.value, identifiers, id_counters)};
    }else if (v.type === "aliases") {
      identifiers = {... identifiers};

      let aliases = v.aliases.map(([n, v]) => {
        let id = ++id_counters.aliases;
        identifiers[n] = {type: 'alias', id: id, opts: v.opts};

        return {id: id, value: v.value, defer: v.opts?.defer ?? 0};
      }).map(v => {
        v.value = analyze_rec(v.value, identifiers, id_counters); // do after since we want to be able to refer to other aliases
        return v;
      });
      return {type: 'aliases', aliases: aliases, value: analyze_rec(v.value, identifiers, id_counters) };
    }else {
      throw `Invalid type ${v.type}`;
    }
  }else{
    throw `Invalid value ${v}`;
  }
}


export default function analyze(v, aliases) {
  let identifiers = {};
  for (let [n, v] of Object.entries(aliases)) { // map to literals
    identifiers[n] = {type: 'literal', opts: v.opts, value: v.value};
  }

  let id_counters = {
    aliases: 0,
    variables: 0,
  };

  return analyze_rec(v, identifiers, id_counters);
}
