import tokenize from './tokenize.js';
import analyze from './analyze.js';
import compile from './compile-eval.js';

export const BASIC = {};
export const COMFORT = {};
export const INFIX = {};

function c(v) {
  let aliases = Object.fromEntries( [BASIC, COMFORT, INFIX].flatMap(x => Object.values(x)).flatMap(x => Object.entries(x)) );
  return compile(analyze(tokenize(v), aliases));
}

function basic(n, o){
  BASIC[n] = Object.fromEntries(Object.entries(o).map(([k, v]) => [k, {value: v}]));
}
function comfort(n, o){
  COMFORT[n] = Object.fromEntries(Object.entries(o).map(([k, v]) => [k, {value: v}]));
}
function infix(n, o){
  INFIX[n] = Object.fromEntries(Object.entries(o).map(([k, [p, a, g, v]]) => {
    let opts = {
      precedence: p,
      associativity: a,
      gobbles: [... g],
    };
    return [k, {opts: opts, value: v}];
  }));
}

basic("constants", {
  "null": null,
  "undefined": undefined,
});

basic("functional", {
  // chain: [a,b,c,d] -> x ->  d(c(b(a(x))))
  "chain": fs => x => fs.reduce((a, f) => f(a), x),
  // uncurry: f -> [a,b,c,d] -> f(a)(b)(c)(d)
  "uncurry": f => xs => xs.reduce((f, a) => f(a), f),
  // ignore second argument A -> B -> A
  "const": x => y => x,
  // swap argument order (A -> B -> C) -> B -> A -> C
  "flip": f => y => x => f(x)(y),
  // identity
  "id": x => x,
  // compose (B -> C) -> (A -> B) -> A -> C
  "compose": g => f => x => g(f(x)),
});
infix("functional", {
  ".": [-100, ">", "<>", c("compose")],
  "$": [-101, ">", "<>", f => x => f(x)], // could have just used id
});

basic("boolean", {
  "true": true,
  "false": false,
  "and": x => y => x && y,
  "or": x => y => x || y,
  "not": x => !x,
  "if": v => a => b => v ? a : b,
});

infix("boolean", {
  '!': [-31, "<", ">", c('not')],
  '&&': [-32, ">", "<>", c('and')],
  '||': [-33, ">", "<>", c('or')],
});

basic("lists", {
  "list.at": prop => x => x[prop],
  "list.length": xs => xs.length,
  "list.all": f => xs => xs.every(f),
  "list.any": f => xs => xs.some(f),
  "list.flatten": xs => xs.flatMap(x => x),
  "list.filter": f => xs => xs.filter(f),
  "list.concat": xs => ys => xs.concat(ys),
  "list.single": x => [x],
  "list.empty": [],
  "list.from": x => Array.from(x),
  "list.prepend": x => xs => [x , ... xs],
  "list.append": x => xs => [...xs, x],

  // with: [a,b,c,d] -> x -> [a(x), b(x), c(x), d(x)]
  // equivalent to flip (compose map (flip id))
  "list.with": fs => x => fs.map(f => f(x)),
  // map: f -> [a,b,c,d] -> [f(a), f(b), f(c), f(d)]
  "list.map": f => xs => xs.map(f),

  // foldr: f -> y -> [a,b,c,d] -> f(a, f(b, f (c, f(d, y))))
  "list.foldr": f => y => xs => xs.reduceRight((a,b) => f(b, a), y),
  // foldl: f -> y -> [a,b,c,d] -> f(f(f(f(y, a), b), c), d)
  "list.foldl": f => y => xs => xs.reduce((a,b) => f(a,b), y),
});
comfort("lists", {
  "map": c('list.map'),
  "length": c('list.length'),
  "with": c('list.with'),
});
infix("lists", {
  '@': [-1, '>', '><', c('list.at')],
  ':': [-2, '<', '<>', c('list.prepend')],
});



basic("objects", {
  "object.at": prop => x => x[prop],
  "object.entries": x => Object.entries(x),
  "object.create": x => Object.fromEntries(x),
  "object.call": prop => x => args => x[prop](... args),
});

comfort("objects", {
  'object': c('object.create'),
});

basic("comparisions", {
  "eq": x => y => x === y,
  "neq": x => y => x !== y,
  "lt": x => y => x < y,
  "gt": x => y => x > y,
  "leq": x => y => x <= y,
  "geq": x => y => x >= y,
});
infix('comparisons', {
  '==': [-20, '>', '<>', c('eq')],
  '!=': [-20, '>', '<>', c('neq')],
  '<' : [-20, '>', '<>', c('lt')],
  '>' : [-20, '>', '<>', c('gt')],
  '<=': [-20, '>', '<>', c('leq')],
  '>=': [-20, '>', '<>', c('geq')],
});

basic('arithmetic', {
  "math.add": x => y => x + y,
  "math.subtract": x => y => x - y,
  "math.multiply": x => y => x * y,
  "math.divide": x => y => x / y,
  "math.power": x => y => Math.pow(x,y),
  "math.remainder": x => y => x % y,
  "floor": x => Math.floor(x),
  "ceil": x => Math.ceil(x),
  "round": x => Math.round(x),
});
infix('arithmetic', {
  '+': [-13, '>', '<>', c('math.add')],
  '-': [-13, '>', '<>', c('math.subtract')],
  '*': [-12, '>', '<>', c('math.multiply')],
  '/': [-12, '>', '<>', c('math.divide')],
  '^': [-11, '>', '<>', c('math.power')],
  '%': [-12, '>', '<>', c('math.remainder')],
})

basic('strings', {
  "string.length": s => s.length(),
  "string.contains": s => sub => s.includes(sub),
});
