// use eval to produce the function


// adds literals to context
function compile_rec(v, context) {
  if (v.type === 'literal') {
    context.push(v.value);
    return `context[${context.length - 1}]`
  }else if(v.type === 'sweep') {
    let result = v.value.map((v, i) => v === null ? `e${i}` : compile_rec(v, context)).map(x => `(${x})`).join('');
    let args = v.value.map((v, i) => v === null ? `e${i} => `: '').join('');
    return args + result;
  }else if(v.type === 'array') {
    return '[' + v.value.map(v => compile_rec(v, context)).join(',') + ']';
  }else if(v.type === 'object') {
    return '({' + Object.entries(v.value).map(([k,v]) => JSON.stringify(k) + ':' + compile_rec(v,context)) + '})';
  }else if(v.type === 'lambda') {
    return `v${v.id} => ${compile_rec(v.value, context)}`;
  }else if(v.type === 'aliases') {
    let string = '(function(){';

    if (v.aliases.length > 0) {
      string = string + '\nlet ' + v.aliases.map(({id}) => `a${id}`).join(', ') + ';';
      for (let {id, value, defer} of v.aliases) {
        string = string
          + `\na${id} = `
          + (new Array(defer)).fill(0).map((_, i) => `e${i} => `).join('')
          + '(' + compile_rec(value, context) + ')'
          + (new Array(defer)).fill(0).map((_, i) => `(e${i})`).join('')
          + ';'
        ;
      }
    }
    string = string
        + '\nreturn ' + compile_rec(v.value, context) + ';' 
        + '\n})()';
    return string;
  }else if(v.type === 'variable') {
    return `v${v.id}`;
  }else if(v.type === 'alias') {
    return `a${v.id}`;
  }else{
    throw `Invalid type ${v.type}`;
  }
}

export default function compile(v){
  let context = [];
  let string = compile_rec(v, context);
  return Function("context", `return ${string};`)(context);
}
