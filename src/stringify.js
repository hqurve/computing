
export default function stringify(v) {
  if (typeof(v) === 'string') {
    return v;
  }else if(Array.isArray(v)) {
    return '(' + v.map(stringify).join(' ') + ')';
  }else if(typeof(v) === 'object') {
    if (v.type === 'literal') {
      return JSON.stringify(v.value);
    }else if(v.type === 'array') {
      return '[' + v.value.map(stringify).join(',') + ']';
    }else if(v.type === 'object') {
      return '{' + Object.entries(v.value).map(([k, v]) => JSON.stringify(k) + ':' + stringify(v)).join(',') + '}';
    }else if(v.type === 'lambda') {
      return '(\\' + v.variable + ' -> ' + stringify(v.value) + ')';
    }else if (v.type === 'aliases') {
      let aliasString = v.aliases.map(([label, opts, expression]) => {
        let props = [
          ['precedence', x => JSON.stringify(x)],
          ['associativity', x => x],
          ['gobbles', x => x.join('')],
          ['defer', x => JSON.stringify(x)],
        ];

        let optString = props.map(([prop, f]) => {
          if (prop in opts) {
            return f(opts[prop]);
          }else{
            return '';
          }
        }).join(',');

        return `;${label};${optString};${stringify(expression)};`;
      }).join('');
      return aliasString + stringify(v.value);
    }else{
      throw `Invalid type ${v.type}`;
    }
  }
}
